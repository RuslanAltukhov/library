<?php
/**
 * Created by PhpStorm.
 * User: Taxphone 1
 * Date: 27.04.2018
 * Time: 12:52
 */

namespace app\models;


use yii\base\Model;

class ScanModel extends Model
{
    public $isbn;
    public $author_full_name;
    public $title;
    public $year;

    public function rules()
    {
        return [
            [['isbn', 'author_full_name', 'title', 'year'], 'required'],
            [['author_full_name', 'title'], 'string'],
            [['year', 'isbn'], 'integer'],
        ];
    }

    public function save()
    {
        $author = Author::createAuthorIfNotExist($this->author_full_name);
        if ($author
            && ($book = Book::createBook(['title' => $this->title, 'year' => $this->year, 'isbn' => $this->isbn]))
            && $author->addBook($book)) {
            return true;
        } else {
            return false;
        }
    }
}