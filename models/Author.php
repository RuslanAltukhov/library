<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "authors".
 *
 * @property int $id
 */
class Author extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'authors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['name', 'string'],
        ];
    }

    public function getBooks()
    {
        return $this->hasMany(Book::class, ['id' => 'book_id'])->viaTable('author_books',
            ['author_id' => 'id']);
    }

    public static function createAuthorIfNotExist(string $name)
    {
        if (self::isAuthorExist($name))
            return self::getAuthorByName($name);
        else
            return self::createAuthor($name);
    }

    public static function createAuthor(string $name)
    {
        $model = new self(['name' => $name]);
        return $model->save() ? $model : null;
    }

    public function addBook($book)
    {
        $this->link("books", $book);
        return true;
    }

    public static function getAuthorByName(string $name)
    {
        return self::findOne(['name' => $name]);
    }

    public static function isAuthorExist(string $name): bool
    {
        return self::find()->where(['name' => $name])->count() > 0;
    }
    public static function getTopAuthors(int $limit = 100)
    {
        return self::find()->select('authors.*')
            ->limit($limit)
            ->withBooksCount()
            ->orderByBooksCount()
            ->asArray()
            ->all();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
        ];
    }

    /**
     * @inheritdoc
     * @return AuthorsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AuthorsQuery(get_called_class());
    }
}
