<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "books".
 *
 * @property int $id
 * @property string $title
 * @property string $isbn
 */
class Book extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'books';
    }

    public function getAuthors()
    {
        return $this->hasMany(Author::class, ['id' => 'author_id'])->viaTable('author_books',
            ['book_id' => 'id']);
    }


    public static function isBookExist(int $isbn): bool
    {
        return self::find()->where(['isbn' => $isbn])->count() > 0;
    }

    public static function createBook($data)
    {
        $model = new Book($data);
        return $model->validate() && $model->save() ? $model : null;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string', 'max' => 200],
            [['isbn', 'year'], 'integer'],
            ['isbn', function () {
                if (self::isBookExist($this->isbn))
                    $this->addError("isbn", "Книга с таким номером уже существует в системе");
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'isbn' => 'Isbn',
        ];
    }

    /**
     * @inheritdoc
     * @return BooksQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BooksQuery(get_called_class());
    }
}
