<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Author]].
 *
 * @see Author
 */
class AuthorsQuery extends \yii\db\ActiveQuery
{
    private $with_books_count = false;

    /**
     * @inheritdoc
     * @return Author[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    public function withBooksCount()
    {
        $this->with_books_count = true;
        $this->addSelect("(SELECT COUNT(id) FROM author_books WHERE author_id=authors.id) AS books_count");
        return $this;
    }

    public function orderByBooksCount()
    {
        if ($this->with_books_count)
            $this->orderBy("books_count DESC");
        return $this;
    }

    /**
     * @inheritdoc
     * @return Author|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
