## Download composer dependencies in project folder with following command:

```
composer install 
```

## Create mysql database:

```
CREATE DATABASE library
```

## Write database params in  **config/db.php** file

```
'dsn' => 'mysql:host=localhost;dbname=library',
    'username' => 'login',
    'password' => 'password',
```
## Apply migrations:

```
php yii migrate
```

## Start build-in web server with command:
```
php yii serve --port=8888
```
### Api methods list

- **/api/send** - store book
- **/api/top-list** - top authors 
- **/api/author-books** - books by author name