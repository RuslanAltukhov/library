<?php

use yii\db\Migration;

/**
 * Handles the creation of table `author_books`.
 */
class m180427_074415_create_author_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('author_books', [
            'id' => $this->primaryKey(),
			'author_id'=>$this->integer(11)->notNull(),
			'book_id'=>$this->integer(11)->notNull()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('author_books');
    }
}
