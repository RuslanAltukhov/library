<?php

use yii\db\Migration;

/**
 * Handles the creation of table `books`.
 */
class m180427_073658_create_books_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('books', [
            'id' => $this->primaryKey(),
            'title' => $this->string(200),
            'year' => $this->bigInteger(13),
            'isbn' => $this->string(13),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('books');
    }
}
