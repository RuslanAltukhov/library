<?php

use yii\db\Migration;

/**
 * Class m180427_114200_add_column_create_date
 */
class m180427_114200_add_column_create_date extends Migration
{
    public $table = "books";
    public $column = "create_date";

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn($this->table, $this->column, $this->timestamp()->defaultExpression("NOW()"));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn($this->table, $this->column);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180427_114200_add_column_create_date cannot be reverted.\n";

        return false;
    }
    */
}
