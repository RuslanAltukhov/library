<?php

namespace app\controllers;

use app\models\Author;
use app\models\ScanModel;
use yii;

class ApiController extends \yii\rest\ActiveController
{
    public $modelClass = "app\models\Book";

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['contentNegotiator'] = [
            'class' => \yii\filters\ContentNegotiator::class,
            'formats' => [
                'application/json' => \yii\web\Response::FORMAT_JSON,
            ],
        ];
        return $behaviors;
    }

    public function actionTopList()
    {
        return Author::getTopAuthors();
    }

    public function actionAuthorBooks()
    {
        $author_name = Yii::$app->request->post("author_name");
        if (!$author_name) {
            throw new yii\web\BadRequestHttpException();
        } else if ($author = Author::getAuthorByName($author_name)) {
            return $author->books;
        } else {
            throw new yii\web\NotFoundHttpException();
        }

    }

    public function actionSend()
    {
        $model = new ScanModel();
        if ($model->load(Yii::$app->request->post(), '') && $model->validate() && $model->save()) {
            return ['code' => 0];
        } else {
            throw new yii\web\BadRequestHttpException(current($model->getErrors()));
        }
    }
}
